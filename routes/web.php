<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\ImageUploadController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\AssetController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\TransactionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [ClientController::class, 'home'])->name('client.homepage');
Route::controller(AuthController::class)->group(function(){
    Route::post('/register', 'register')->name('register');
    Route::post('/login', 'login')->name('login');
    Route::get('/logout', 'logout')->name('logout');
});
Route::get('/booking', [ClientController::class, 'booking'])->name('client.booking');
Route::get('/booking/{id}/room', [ClientController::class, 'roomShow'])->name('client.booking.room');
Route::post('/booking', [ClientController::class, 'bookingStore'])->name('client.booking.store');

Route::middleware('client')->group(function () {
    Route::get('/bookings', [BookingController::class, 'bookings'])->name('client.booking.list');
    Route::get('/booking/{code}/payment', [PaymentController::class, 'viewPaymentBooking'])->name('client.booking.payment');
    Route::post('/booking/vnpay/payment', [PaymentController::class, 'checkout'])->name('client.booking.payment.vnpay');
    Route::get('/payment/return', [PaymentController::class, 'paymentRedirect'])->name('client.booking.payment.return');
    Route::get('/transactions', [TransactionController::class, 'indexClient'])->name('client.transactions.list');
});

Route::post('image/upload/store',[ImageUploadController::class, 'fileStore'])->name('upload.image');
Route::post('image/delete',[ImageUploadController::class, 'fileDestroy'])->name('delete.image');
Route::post('/get-image-update',[ImageUploadController::class, 'imageUpdateData'])->name('update.image.data');


Route::middleware('admin')->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/', [AdminController::class, 'dashboard'])->name('admin.dashboard');

        Route::get('/clients', [AdminController::class, 'clients'])->name('admin.client.list');
        Route::get('/client/{id}/edit', [AdminController::class, 'activeOrInactiveUser'])->name('admin.client.edit');

        Route::get('/partners', [AdminController::class, 'partners'])->name('admin.partner.list');
        Route::get('/partner/{id}/edit', [AdminController::class, 'activeOrInactiveUser'])->name('admin.partner.edit');
        Route::get('/locations', [AdminController::class, 'locations'])->name('admin.location.list');
        Route::delete('/locations/{id}/delete', [AdminController::class, 'deleteLocation'])->name('admin.location.delete');

        Route::get('/assets', [AdminController::class, 'assets'])->name('admin.asset.list');
        Route::get('/asset-blocks', [AdminController::class, 'assetBlock'])->name('admin.asset.block.list');
        Route::get('/asset-restore/{id}', [AdminController::class, 'restoreAsset'])->name('admin.asset.restore');
        Route::delete('/assets/{id}', [AdminController::class, 'blockAsset'])->name('admin.asset.block');

        Route::get('/rooms', [AdminController::class, 'rooms'])->name('admin.room.list');
        Route::get('/room-blocks', [AdminController::class, 'roomBlock'])->name('admin.room.block.list');
        Route::get('/room-restore/{id}', [AdminController::class, 'restoreRoom'])->name('admin.room.restore');
        Route::delete('/rooms/{id}', [AdminController::class, 'blockRoom'])->name('admin.room.block');

        Route::get('/bookings', [AdminController::class, 'bookings'])->name('admin.bookings.index');
        Route::get('/transactions', [AdminController::class, 'transactions'])->name('admin.transactions.index');
    });
});

Route::middleware('partner')->group(function () {
    Route::prefix('partners')->group(function () {
        Route::get('/', [PartnerController::class, 'dashboard'])->name('partner.dashboard');
        Route::resource('rooms', RoomController::class);
        Route::resource('assets', AssetController::class);
        Route::get('/bookings', [BookingController::class, 'index'])->name('bookings.index');
    });
});

Route::controller(GoogleController::class)->group(function(){
    Route::get('auth/google', 'redirectToGoogle')->name('auth.google');
    Route::get('auth/google/callback', 'handleGoogleCallback');
});
