<div class="modal fade log-reg" id="exampleModal" tabindex="-1" aria-hidden="true" style="overflow: scroll;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="post-tabs">
                    <ul class="nav nav-tabs nav-pills nav-fill" id="postsTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button aria-controls="login" aria-selected="false" class="nav-link active" data-bs-target="#login" data-bs-toggle="tab" id="login-tab" role="tab" type="button">Login</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button aria-controls="register" aria-selected="true" class="nav-link" data-bs-target="#register" data-bs-toggle="tab" id="register-tab" role="tab" type="button">Register</button>
                        </li>
                    </ul>

                    <div class="tab-content blog-full" id="postsTabContent">
                        <div aria-labelledby="login-tab" class="tab-pane fade active show" id="login" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="blog-image rounded">
                                        <a href="#" style="background-image: url(https://vietnam.travel/sites/default/files/2021-11/Website%20PR%20Press%20release%20background_0.PNG);"></a>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <h4 class="text-center border-b pb-2">Login</h4>
                                    <div class="log-reg-button align-items-center">
                                        <a href="{{ route('auth.google') }}" class="btn btn-google w-100"><i class="fab fa-google"></i> Login with Google</a>
                                    </div>
                                    <hr class="log-reg-hr position-relative my-4 overflow-visible" />
                                    <form id="form-login">
                                        <div class="form-group mb-2">
                                            <input type="email" name="email" class="form-control" id="email" placeholder="Email Address" />
                                        </div>
                                        <div class="form-group mb-2">
                                            <input type="password" name="password" class="form-control" id="password" placeholder="Password" />
                                        </div>
                                        <div class="form-group mb-2">
                                            <input type="checkbox" class="custom-control-input" id="exampleCheck" checked/>
                                            <label class="custom-control-label mb-0" for="exampleCheck1">Remember me</label>
                                            <a class="float-end" href="#">Lost your password?</a>
                                        </div>
                                        <div class="comment-btn mb-2 pb-2 text-center border-b">
                                            <button type="submit" class="nir-btn w-100" id="submit-form-login">Login
                                                <div class="spinner-border spin-loading-block" id="spin-loading" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                            </button>
                                        </div>
                                        <p class="text-center">Don't have an account? <a href="#" class="theme">Register</a></p>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div aria-labelledby="register-tab" class="tab-pane fade" id="register" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="blog-image rounded">
                                        <a href="#" style="background-image: url(https://vietnam.travel/sites/default/files/2021-11/Website%20PR%20Press%20release%20background_0.PNG);"></a>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <h4 class="text-center border-b pb-2">Register</h4>
                                    <div class="log-reg-button align-items-center">
                                        <a href="{{ route('auth.google') }}" class="btn btn-google w-100"><i class="fab fa-google"></i> Login with Google</a>
                                    </div>
                                    <hr class="log-reg-hr position-relative my-4 overflow-visible" />
                                    <form id="form-register">
                                        <div class="form-group mb-2">
                                            <input type="text" name="name" class="form-control" id="name_register" placeholder="User Name" />
                                        </div>
                                        <div class="form-group mb-2">
                                            <input type="email" name="email" class="form-control" id="email_register" placeholder="Email Address" />
                                        </div>
                                        <div class="form-group mb-2">
                                            <input type="password" name="password" class="form-control" id="password_register" placeholder="Password" />
                                        </div>
                                        <div class="form-group mb-2 d-flex">
                                            <input type="checkbox" class="custom-control-input" id="exampleCheck1" checked/>
                                            <label class="custom-control-label mb-0 ms-1 lh-1" for="exampleCheck1">I have read and accept the Terms and Privacy Policy?</label>
                                        </div>
                                        <div class="comment-btn mb-2 pb-2 text-center border-b">
                                            <button type="submit" class="nir-btn w-100" id="submit-form-register">Register
                                                <div class="spinner-border spin-loading-block" id="spin-loading" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                            </button>
                                        </div>
                                        <p class="text-center">Already have an account? <a href="#" class="theme">Login</a></p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#submit-form-login').on('click',function(e) {
        e.preventDefault();
        $("#spin-loading").removeClass("spin-loading-block")
        $.ajax({
            url: '{{route('login')}}',
            type: "post",
            data:{
                "_token": "{{ csrf_token() }}",
                email : $('#email').val(),
                password : $('#password').val(),
            },
            success:function(response){
                $("#spin-loading").addClass("spin-loading-block")
                if(!response.status){
                    swal("Login error", response.message , "error");
                }else {
                    location.reload();
                }

            }
        });
    });

    $('#submit-form-register').on('click',function(e) {
        e.preventDefault();
        $("#spin-loading").removeClass("spin-loading-block")
        $.ajax({
            url: '{{route('register')}}',
            type: "post",
            data:{
                "_token": "{{ csrf_token() }}",
                name : $('#name_register').val(),
                email : $('#email_register').val(),
                password : $('#password_register').val(),
            },
            success:function(response){
                console.log(response)
                $("#spin-loading").addClass("spin-loading-block")
                swal("Done booking", "register success" ,"success");
                location.reload();
            }
        });
    });
</script>
