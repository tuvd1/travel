<footer class="pt-20 pb-4" style="background-image: url(http://travel.test/client/images/background_pattern.png);">
    <div class="section-shape top-0" style="background-image: url(http://travel.test/client/images/shape8.png);"></div>

    <div class="insta-main pb-10">
        <div class="container">
            <div class="insta-inner">
                <div class="follow-button">
                    <h5 class="m-0 rounded"><i class="fab fa-instagram"></i> Follow on Instagram</h5>
                </div>
                <div class="row attract-slider">
                    <div class="col-md-3 col-sm-6">
                        <div class="insta-image rounded">
                            <a href="gallery.html"><img src="{{asset('client')}}/images/insta/ins-3.jpg" alt="insta" /></a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="insta-image rounded">
                            <a href="gallery.html"><img src="{{asset('client')}}/images/insta/ins-4.jpg" alt="insta" /></a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="insta-image rounded">
                            <a href="gallery.html"><img src="{{asset('client')}}/images/insta/ins-5.jpg" alt="insta" /></a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="insta-image rounded">
                            <a href="gallery.html"><img src="{{asset('client')}}/images/insta/ins-1.jpg" alt="insta" /></a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="insta-image rounded">
                            <a href="gallery.html"><img src="{{asset('client')}}/images/insta/ins-7.jpg" alt="insta" /></a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="insta-image rounded">
                            <a href="gallery.html"><img src="{{asset('client')}}/images/insta/ins-8.jpg" alt="insta" /></a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="insta-image rounded">
                            <a href="gallery.html"><img src="{{asset('client')}}/images/insta/ins-2.jpg" alt="insta" /></a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="insta-image rounded">
                            <a href="gallery.html"><img src="{{asset('client')}}/images/insta/ins-6.jpg" alt="insta" /></a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="insta-image rounded">
                            <a href="gallery.html"><img src="{{asset('client')}}/images/insta/ins-9.jpg" alt="insta" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-upper pb-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 mb-4 pe-4">
                    <div class="footer-about">
                        <img src="{{asset('client')}}/images/logo-white.png" alt="" />
                        <p class="mt-3 mb-3 white">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Odio suspendisse leo neque iaculis molestie sagittis maecenas aenean eget molestie sagittis.
                        </p>
                        <ul>
                            <li class="white"><strong>PO Box:</strong> +47-252-254-2542</li>
                            <li class="white"><strong>Location:</strong> Collins Street, sydney, Australia</li>
                            <li class="white">
                                <strong>Email:</strong> <a href="https://htmldesigntemplates.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="4e272028210e1a3c2f382b222720602d2123">[email&#160;protected]</a>
                            </li>
                            <li class="white"><strong>Website:</strong> www.Travelin.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12 mb-4">
                    <div class="footer-links">
                        <h3 class="white">Quick link</h3>
                        <ul>
                            <li><a href="about-us.html/index.html">About Us</a></li>
                            <li><a href="about-us.html/index.html">Delivery Information</a></li>
                            <li><a href="about-us.html/index.html">Privacy Policy</a></li>
                            <li><a href="about-us.html/index.html">Terms &amp; Conditions</a></li>
                            <li><a href="about-us.html/index.html">Customer Service</a></li>
                            <li><a href="#about-us.html">Return Policy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12 mb-4">
                    <div class="footer-links">
                        <h3 class="white">Categories</h3>
                        <ul>
                            <li><a href="about-us.html/index.html">Travel</a></li>
                            <li><a href="about-us.html/index.html">Technology</a></li>
                            <li><a href="about-us.html/index.html">Lifestyle</a></li>
                            <li><a href="about-us.html/index.html">Destinations</a></li>
                            <li><a href="about-us.html/index.html">Entertainment</a></li>
                            <li><a href="about-us.html/index.html">Business</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                    <div class="footer-links">
                        <h3 class="white">Newsletter</h3>
                        <div class="newsletter-form">
                            <p class="mb-3">Jin our community of over 200,000 global readers who receives emails filled with news, promotions, and other good stuff.</p>
                            <form action="#" method="get" accept-charset="utf-8" class="border-0 d-flex align-items-center">
                                <input type="text" placeholder="Email Address" />
                                <button class="nir-btn ms-2">Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="copyright-inner rounded p-3 d-md-flex align-items-center justify-content-between">
                <div class="copyright-text">
                    <p class="m-0 white">2022 Travelin. All rights reserved.</p>
                </div>
                <div class="social-links">
                    <ul>
                        <li>
                            <a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="particles-js"></div>
</footer>
