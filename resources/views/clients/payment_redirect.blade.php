@extends('layouts.client')
@section('title', $title)
@section('contents')
<section class="ftco-section goto-here">
    <div class="container">
        <div class="header clearfix text-center">
            <h3 class="text-muted">
                {{Request::get('vnp_ResponseCode') == VNP_SUCCESS ? 'TRANSACTION SUCCESSFUL!' : 'TRANSACTION ERROR!'}}
            </h3>
            <p>Transaction failed due to: Customer cancels transaction</p>
        </div>
        <div class="text-center">
            @if(Request::get('vnp_ResponseCode') == VNP_SUCCESS)
                <i class="fa fa-check-circle text-success" style="font-size: 3rem"></i>
            @else
                <i class="fa fa-exclamation-circle text-danger" style="font-size: 3rem"></i>
            @endif
        </div>
        <div class="table-responsive text-center mt-5">
            <div class="form-group">
                <label>Code transaction:</label>
                <label class="fw-bold">#{{Request::get('vnp_TxnRef')}}</label>
            </div>
            <div class="form-group">
                <label>Money payment:</label>
                <label>{{number_format(Request::get('vnp_Amount')/100)}} đ</label>
            </div>
            <div class="form-group">
                <label>Description:</label>
                <label>{{Request::get('vnp_OrderInfo')}}</label>
            </div>
            <div class="form-group">
                <label>Code payment in VNPAY:</label>
                <label>{{Request::get('vnp_TransactionNo')}}</label>
            </div>
            <div class="form-group">
                <label>Code shocking:</label>
                <label>{{Request::get('vnp_BankCode')}}</label>
            </div>
            <div class="form-group">
                <label>Time:</label>
                <label>{{Request::get('vnp_PayDate')}}</label>
            </div>
            <div class="form-group mt-5">
                <a href="{{route('client.booking.list')}}" class="nir-btn">View list booking</a>
            </div>
        </div>
    </div>
</section>
@endsection
