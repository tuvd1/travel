@extends('layouts.client')
@section('title', $title)
@section('contents')
    <section class="trending pt-6 pb-0 bg-lgrey">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12 mb-4">
                    <div class="payment-book">
                        <div class="booking-box">
                            <div class="booking-box-title d-md-flex align-items-center bg-title p-4 mb-4 rounded text-md-start text-center">
                                <i class="fa fa-check px-4 py-3 bg-white rounded title fs-5"></i>
                                <div class="title-content ms-md-3">
                                    <h3 class="mb-1 white">Thank You. Your booking order is confirmed now.</h3>
                                    <p class="mb-0 white">A confirmation email has been sent to your provided email address.</p>
                                </div>
                            </div>
                            <div class="travellers-info mb-4">
                                <h4>Traveler Information</h4>
                                <table>
                                    <tr>
                                        <td>Booking Code</td>
                                        <td>#{{$booking->code}}</td>
                                    </tr>
                                    <tr>
                                        <td>Full Name</td>
                                        <td>{{$booking->user->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Email Address</td>
                                        <td>{{auth()->user()->email}}</td>
                                    </tr>
                                    <tr>
                                        <td>Check in</td>
                                        <td>{{$booking->start_date}}</td>
                                    </tr>
                                    <tr>
                                        <td>Check out</td>
                                        <td>{{$booking->end_date}}</td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td>{{number_format($booking->total_money)}} đ</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="booking-border mb-4">
                                <h4 class="border-b pb-2 mb-2">Payment</h4>
                                <form action="{{route('client.booking.payment.vnpay')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="code" value="{{$booking->code}}">
                                    <input type="hidden" name="infor" value="Payment room  {{$booking->code}}">
                                    <input type="hidden" name="amount" value="{{$booking->total_money}}">
                                    <button name="redirect" class="nir-btn me-2" type="submit"><i class="fa fa-wallet"></i> VN PAY</button>
                                    <button class="nir-btn-black" type="button"><i class="fa fa-bank"></i> Bank transfer</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection
