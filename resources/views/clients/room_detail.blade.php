@extends('layouts.client')
@section('title', $title)
@section('styles')
    <style>
        .spinner-border{
            width: 1rem !important;
            height: 1rem !important;
        }
        .spin-loading-block{
            display: none !important;
        }
    </style>
@endsection
@section('contents')
    <section class="breadcrumb-main pb-20 pt-14" style="background-image: url(http://travel.test/client/images/bg/bg1.jpg);">
        <div class="section-shape section-shape1 top-inherit bottom-0" style="background-image: url(http://travel.test/client/images/shape8.png);"></div>
        <div class="breadcrumb-outer">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h1 class="mb-3">Room Detail</h1>
                    <nav aria-label="breadcrumb" class="d-block">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Room Detail</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="dot-overlay"></div>
    </section>

    <section class="trending pt-6 pb-0 bg-lgrey">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-content">
                        <div class="single-full-title border-b mb-2 pb-2">
                            <div class="single-title">
                                <h2 class="mb-1">{{$room->asset->name}}</h2>
                                <div class="rating-main d-lg-flex align-items-center text-lg-start text-center">
                                    <p class="mb-0 me-2"><i class="icon-location-pin"></i> {{$room->asset->district}}, {{$room->asset->province}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="description-images mb-4 overflow-hidden">
                            <div class="thumbnail-images position-relative">
                                <div class="slider-store rounded overflow-hidden">
                                    @foreach($room->image as $item)
                                    <div>
                                        <img src="/images/{{$item->image_path}}" alt="1" width="850px" height="560px" style="object-fit: cover"/>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="slider-thumbs">
                                    @foreach($room->image as $item)
                                        <div>
                                            <img src="/images/{{$item->image_path}}" alt="1" width="150px" height="150px" style="object-fit: cover"/>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="description mb-2">
                            <h4>Description</h4>
                            <p>{{$room->description}}</p>
                        </div>
                        <div class="tour-includes mb-4">
                            <table>
                                <tbody>
                                    <tr>
                                        <td><i class="fas fa-bed-alt"></i> {{$room->bed}} Bed</td>
                                        <td><i class="fa fa-group pink mr-1" aria-hidden="true"></i> Max People : {{$room->amount_of_people}}</td>
                                        <td><i class="fad fa-refrigerator"></i> {{$room->fridge}} Fridge</td>
                                    </tr>
                                    <tr>
                                        <td><i class="far fa-shower"></i> {{$room->bathroom}} bathroom</td>
                                        <td><i class="fad fa-utensils"></i> {{config('common')['breakfast'][$room->breakfast]['name']}}</td>
                                        <td><i class="fad fa-chart-area"></i> {{$room->area}}m2</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="col-lg-4 ps-lg-4">
                    <div class="sidebar-sticky">
                        <div class="list-sidebar">
                            <div class="sidebar-item">
                                <div class="form-content rounded bg-title" >
                                    <h4 class="white text-center border-b pb-2">MAKE A BOOKING</h4>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group mb-2">
                                                <label class="white">Check-in</label>
                                                <div class="input-box">
                                                    <i class="fad fa-clock"></i>
                                                    <input type="date" name="start_date" id="start_date" value="{{old('start_date')}}" required>
                                                </div>
                                            </div>
                                            <p id="error_start_date" class="text-danger" style="font-size: 13px"></p>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group mb-2">
                                                <label class="white">Check-out</label>
                                                <div class="input-box">
                                                    <i class="fad fa-clock"></i>
                                                    <input type="date" name="end_date" id="end_date" value="{{old('end_date')}}" required>
                                                </div>
                                            </div>
                                            <p id="error_end_date" class="text-danger" style="font-size: 13px"></p>
                                        </div>
                                        <input type="hidden" name="room_id" id="room_id" value="{{$room->id}}">
                                        <input type="hidden" name="total_price" id="total_price" value="">
                                        <div class="col-lg-12">
                                            <div class="form-group mb-0">
                                                @if(auth()->check())
                                                <button class="nir-btn w-100" id="btn-open-model">
                                                    Book now
                                                </button>
                                                @else
                                                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal" class="nir-btn white">
                                                        Login
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(auth()->check())
    <div class="modal fade log-reg" id="orderDetail" tabindex="-1" aria-hidden="true" style="overflow: scroll;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="post-tabs">
                        <section class="trending pt-6 pb-0 bg-lgrey">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12 mb-4">
                                        <div class="payment-book">
                                            <div class="booking-box">
                                                <div class="customer-information mb-4">
                                                    <h3 class="border-b pb-2 mb-2">Traveller Information</h3>
                                                    <div class="mb-2">
                                                        <h5>Let us know who you are</h5>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group mb-2">
                                                                    <label>First Name</label>
                                                                    <input type="text" id="first-name" placeholder="First Name">
                                                                </div>
                                                                <p id="error_first_name" class="text-danger" style="font-size: 13px"></p>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group mb-2">
                                                                    <label>Last Name</label>
                                                                    <input type="text" id="last-name" placeholder="Last Name">
                                                                </div>
                                                                <p id="error_last_name" class="text-danger" style="font-size: 13px"></p>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group mb-2">
                                                                    <label>Email</label>
                                                                    <input type="email" id="email" placeholder="Email Address" value="{{auth()->user()->email}}" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group mb-2">
                                                                    <label>Phone</label>
                                                                    <input type="text" id="phone" placeholder="Please enter your phone number!">
                                                                </div>
                                                                <p id="error_phone" class="text-danger" style="font-size: 13px"></p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="customer-information mb-4 d-flex align-items-center bg-grey rounded p-4">
                                                    <i class="fa fa-grin-wink rounded fs-1 bg-theme white p-3 px-4"></i>
                                                    <div class="customer-info ps-4">
                                                        <h6 class="mb-1">Good To Know:</h6>
                                                        <small>Free cancellation after 1 day</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 ">
                                        <div class="sidebar-sticky">
                                            <div class="sidebar-item bg-white rounded box-shadow overflow-hidden p-3 mb-4">
                                                <h4>Your Booking Details</h4>
                                                <div class="trend-full border-b pb-2 mb-2">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4">
                                                            <div class="trend-item2 rounded">
                                                                <a style="background-image: url(/images/{{$room->image[0]->image_path}});"></a>
                                                                <div class="color-overlay"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 ps-0">
                                                            <div class="trend-content position-relative">
                                                                <h5 class="mb-1"><a>{{$room->asset->name}}</a></h5>
                                                                <h6 class="theme mb-0"><i class="icon-location-pin"></i> {{$room->asset->district}}, {{$room->asset->province}}</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="trend-check border-b pb-2">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="trend-check-item bg-grey rounded p-3 mb-2">
                                                                <p class="mb-0">Check In</p>
                                                                <h6 class="mb-0"><span id="checkin-date"></span></h6>
                                                                <small>10:00 - 14:00</small>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="trend-check-item bg-grey rounded p-3 mb-2">
                                                                <p class="mb-0">Check Out</p>
                                                                <h6 class="mb-0"><span id="checkout-date"></span></h6>
                                                                <small>10:00 - 14:00</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="trend-check border-b pb-2 mb-2">
                                                    <p class="mb-0">Total Length of Stay:</p>
                                                    <h6 class="mb-0"><span id="total-day"></span></h6>
                                                </div>
                                            </div>
                                            <div class="sidebar-item bg-white rounded box-shadow overflow-hidden p-3 mb-4">
                                                <h4>Your Price Summary</h4>
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <td> Room price / Day</td>
                                                        <td class="theme2"><span id="price-room"></span></td>
                                                    </tr>

                                                    <tr>
                                                        <td> Number date</td>
                                                        <td class="theme2"><span id="date-number-room"></span></td>
                                                    </tr>

                                                    <tr>
                                                        <td>Tax & fee</td>
                                                        <td class="theme2"><span id="price_in_tax"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Booking Fee</td>
                                                        <td class="theme2">Free</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td class="theme2"><span id="price_total"></span></td>
                                                    </tr>
                                                    </tbody>
                                                    <tfoot class="bg-title">
                                                    <tr>
                                                        <th class="font-weight-bold white">Amount</th>
                                                        <th class="font-weight-bold white"><span id="price_amount"></span></th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                                <button class="nir-btn white mt-2" style="width: 100%;" id="booking-request-button">
                                                    Booking now
                                                    <div class="spinner-border spin-loading-block" id="spin-loading" role="status">
                                                        <span class="sr-only">Loading...</span>
                                                    </div>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
@section('scripts')
    <script>
        $('#end_date').change(function() {
            var start = moment($('#start_date').val());
            var end = moment($('#end_date').val());
            var today = moment(new Date().toISOString().split('T')[0]);
            if($('#start_date').val() == ""){
                $("#error_start_date").text("The start date cannot be left blank!")
                $('#end_date').val("");
                $('#btn-open-model').attr('data-bs-toggle','');
                $('#btn-open-model').attr('data-bs-target','');
            }else {
                $("#error_start_date").text("")
                if(start.diff(today, "days") <= 0){
                    $("#error_start_date").text("Not selected before today!")
                    $('#end_date').val("")
                    $('#btn-open-model').attr('data-bs-toggle','');
                    $('#btn-open-model').attr('data-bs-target','');
                }else {
                    if (end.diff(start, "days") <= 0) {
                        $("#error_end_date").text("End date must be greater than start date")
                        $('#btn-open-model').attr('data-bs-toggle','');
                        $('#btn-open-model').attr('data-bs-target','');
                    } else {
                        $("#error_end_date").text("")
                        $('#btn-open-model').attr('data-bs-toggle','modal');
                        $('#btn-open-model').attr('data-bs-target','#orderDetail');
                    }
                }
            }
            var price_date = '{{$room->price}}' * end.diff(start, "days")
            var price_tax = price_date/100*10
            var price_total = price_date + price_tax
            var price_total_show = price_date + price_tax
            if (price_date < 0){
                $("#price_in_date").text('0đ');
                $("#price_in_tax").text('0đ');
                $("#price_total").text('0đ');
            }else{
                $("#price_in_date").text(price_date.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + 'đ');
                $("#price_in_tax").text(price_tax.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + 'đ');
                $("#price_total").text(price_total_show.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + 'đ');
                $("#price_amount").text(price_total_show.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + 'đ');
                $("#date-number-room").text(end.diff(start, "days") + " days")
                $("#checkin-date").text($('#start_date').val())
                $("#checkout-date").text($('#end_date').val())
                $("#total-day").text(end.diff(start, "days") + " Days")
                $("#price-room").text('{{$room->price}}'.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + "đ")
            }
            $('#total_price').val(price_total)

            $('#booking-request-button').on('click',function(e) {
                $("#spin-loading").removeClass("spin-loading-block")

                if($('#first-name').val() == ''){
                    $("#error_first_name").text("First name is not blank!")
                    $("#spin-loading").addClass("spin-loading-block")
                    var valiFirst = false
                }else{
                    $("#error_first_name").text("")
                    var valiFirst = true
                }
                if($('#last-name').val() == ''){
                    $("#error_last_name").text("Last name is not blank!")
                    $("#spin-loading").addClass("spin-loading-block")
                    var valiLast = false
                }else{
                    $("#error_last_name").text("")
                    var valiLast = true
                }
                if($('#phone').val() == ''){
                    $("#spin-loading").addClass("spin-loading-block")
                    $("#error_phone").text("Phone is not blank!")
                    var valiPhone = false
                }else{
                    if (/^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/.test($('#phone').val())) {
                        $("#error_phone").text("")
                        var valiPhone = true
                    }else {
                        $("#error_phone").text("invalid phone number!")
                        $("#spin-loading").addClass("spin-loading-block")
                        var valiPhone = false
                    }
                }
                e.preventDefault();
                if(valiFirst && valiLast && valiPhone){
                    $.ajax({
                        url: '{{route('client.booking.store')}}',
                        type: "post",
                        data:{
                            "_token": "{{ csrf_token() }}",
                            first_name : $('#first-name').val(),
                            last_name : $('#last-name').val(),
                            start_date : $('#start_date').val(),
                            end_date : $('#end_date').val(),
                            phone : $('#phone').val(),
                            total_price : $('#total_price').val(),
                            room_id : $('#room_id').val(),
                        },
                        success:function(response){
                            console.log(response)
                            $("#spin-loading").addClass("spin-loading-block")
                            if(response.status) {
                                swal("Done booking", response.message, "success");
                                setTimeout(window.location.replace("http://127.0.0.1:8000/booking/" + response.data_booking.code + "/payment"), 10000);
                            }else{
                                if(response.message.phone) {
                                    swal("Error phone", response.message.phone[0], "error");
                                }else{
                                    $("#error_phone").text("")
                                }
                            }
                        }
                    });
                }
            });
        });


    </script>
@endsection
