@extends('layouts.client')
@section('title', $title)
@section('styles')
    <style>
        .page-link{
            color: #019e9d !important;
        }
        .page-item.active .page-link {
            color: #fff !important;
            background-color: #019e9d !important;
            border-color: #019e9d !important;
        }
    </style>
@endsection
@section('contents')
    <section class="breadcrumb-main pb-20 pt-14" style="background-image: url(http://travel.test/client/images/bg/bg1.jpg);">
        <div class="section-shape section-shape1 top-inherit bottom-0" style="background-image: url(http://travel.test/client/images/shape8.png);"></div>
        <div class="breadcrumb-outer">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h1 class="mb-3">Bookings</h1>
                    <nav aria-label="breadcrumb" class="d-block">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Booking</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="dot-overlay"></div>
    </section>

    <div class="form-main mt-7">
        <div class="section-shape top-0" style="background-image: url(images/shape-pat.png);"></div>
        <div class="container">
            <div class="row align-items-center form-content rounded position-relative me-5" style="width: 100%">
                <div class="col-lg-2 p-0">
                    <h4 class="form-title form-title1 text-center p-4 py-5 white bg-theme mb-0 rounded-start d-lg-flex align-items-center"><i class="icon-location-pin fs-1 me-1"></i> Find Your Holidays</h4>
                </div>
                <div class="col-lg-10 px-4">
                    <form class="form-content-in d-lg-flex align-items-center">
                        <div class="form-group me-2">
                            <div class="input-box">
                                <label>Location</label>
                                <select class="niceSelect" id="province">
                                    <option value="">Option!</option>
                                    @foreach($assets as $items)
                                    <option value="{{$items->province}}" {{$oldProvince == $items->province ? 'selected' : ''}}>{{$items->province}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group me-2">
                            <div class="input-box">
                                <label>Number of people</label>
                                <input type="number" id="amount_of_people" placeholder="Please enter number of people" value="{{$oldPeople ?? ''}}">
                            </div>
                        </div>
                        <div class="form-group mb-0 text-center">
                            <button id="button_filter_booking" class="nir-btn w-100" style="top: 18px"><i class="fa fa-search mr-2"></i> Search Now</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <section class="trending pt-6 pb-0 bg-lgrey">
        <div class="container">
            <div class="row flex-row-reverse">
                <div class="col-lg-12">
                    <div class="destination-list">
                        @forelse($rooms as $value)
                            @if($value->asset)
                            <div class="trend-full bg-white rounded box-shadow overflow-hidden p-4 mb-4">
                                <div class="row">
                                    <div class="col-lg-4 col-md-3">
                                        <div class="trend-item2 rounded">
                                            <a href="{{route('client.booking.room', $value->id)}}" style="background-image: url('images/{{$value->image[0]->image_path}}');"></a>
                                            <div class="color-overlay"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-6">
                                        <div class="trend-content position-relative text-md-start text-center">
                                            <h3 class="mb-1"><a href="{{route('client.booking.room', $value->id)}}">{{$value->asset->name ?? ""}}</a></h3>
                                            <h6 class="theme mb-0"><i class="icon-location-pin"></i> {{$value->asset->province ?? ""}}</h6>
                                            <p class="mt-4 mb-0">
                                                Taking Safety Measures <br />
                                                <a href="#"><span class="theme"> Free cancellation</span></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="trend-content text-md-end text-center">
                                            <div class="trend-price my-2">
                                                <span class="mb-0">From</span>
                                                <h3 class="mb-0">{{number_format($value->price)}}đ</h3>
                                                <small>/ Night</small>
                                            </div>
                                            <a href="{{route('client.booking.room', $value->id)}}" class="nir-btn">View Detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @empty
                            <div class="trend-full bg-white rounded box-shadow overflow-hidden p-4 mb-4">No room</div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="trending pt-2 pb-0 bg-lgrey">
        <div class="container">
            <div class="row flex-row-reverse">
                <div class="col-lg-12">
                    @include('clients.components.pagination', ['datas' => $rooms])
                </div>
            </div>
        </div>
    </section>

@endsection
@section('scripts')
    <script>
        $('#button_filter_booking').on('click',function(e) {
            var province = $('#province').val();
            var people = $('#amount_of_people').val();
            window.location.replace("?province=" + province + "&people=" + people)
        });
    </script>
@endsection
