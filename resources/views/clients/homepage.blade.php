@extends('layouts.client')
@section('title', $title)
@section('contents')
    <section class="banner overflow-hidden">
        <div class="banner-main">
            <div class="banner-image">
                <div class="video-banner">
                    <video autoplay muted loop id="vid" class="w-100">
                        <source src="{{asset('client')}}/images/travel.mp4" type="video/mp4" />
                    </video>
                </div>
                <div class="dot-overlay"></div>
            </div>
            <div class="banner-content">
                <div class="entry-meta mb-0">
                    <span class="entry-category"><a href="tour-grid.html" class="white"> Travelin</a></span>
                </div>
                <h1 class="mb-0"><a href="{{route('client.booking')}}" class="white">Create Your Journey Joyfull Through Us</a></h1>
            </div>
        </div>
    </section>

    <section class="trending pb-6" style="background-image: url(http://travel.test/images/shape4.png);">
        <div class="container">
            <div class="row align-items-center justify-content-between mb-6">
                <div class="col-lg-7">
                    <div class="section-title text-center text-lg-start">
                        <h4 class="mb-1 theme1">Top Deals</h4>
                        <h2 class="mb-1">The Last <span class="theme">Minute Deals</span></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                    </div>
                </div>
                <div class="col-lg-5"></div>
            </div>
            <div class="trend-box">
                <div class="row item-slider">
                    @foreach($rooms as $value)
                    <div class="col-lg-4">
                        <div class="trend-item1 rounded box-shadow bg-white mb-4">
                            <div class="trend-image position-relative">
                               <img src="images/{{$value->image[0]->image_path}}" alt="image" style="height: 450px;width: 100%;object-fit: cover;" class="" />
                                <div class="trend-content1 p-4">
                                    <h5 class="theme1 mb-1"><i class="flaticon-location-pin"></i> Viet Nam</h5>
                                    <h3 class="mb-1 white"><a href="{{route('client.booking.room', $value->id)}}" class="white">{{$value->asset->province ?? ""}}</a></h3>
                                    <div class="entry-meta d-flex align-items-center justify-content-between">
                                        <div class="entry-author d-flex align-items-center">
                                            <p class="mb-0 white"><span class="theme1 fw-bold fs-5"> {{number_format($value->price)}} đ</span> | Night</p>
                                        </div>
                                        <div class="entry-author">

                                        </div>
                                    </div>
                                </div>
                                <div class="overlay"></div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection
