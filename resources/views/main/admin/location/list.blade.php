@extends('layouts.main')
@section('styles')
    <link href="{{asset('main')}}/vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
@endsection
@section('contents')
    <div class="d-flex justify-content-between align-items-center flex-wrap">
        <div class="card-action coin-tabs mb-3">
            <div class="newest ms-3">
                <select class="default-select">
                    <option>Newest</option>
                    <option>Oldest</option>
                </select>
            </div>
        </div>
        <div class="d-flex align-items-center mb-3">

        </div>
    </div>
    <div class="row mt-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="tab-content">
                        <div class="tab-pane active show" >
                            <div class="table-responsive">
                                <table class="table card-table display mb-4 shadow-hover default-table table-responsive-lg dataTable no-footer" id="guestTable-all">
                                    <thead>
                                    <tr>
                                        <th class="bg-none">
                                            <div class="form-check style-1">
                                                <input class="form-check-input" type="checkbox" value="" id="checkAll">
                                            </div>
                                        </th>
                                        <th>Tên địa danh</th>
                                        <th>Thành phố</th>
                                        <th>Ngày thêm</th>
                                        <th class="bg-none">Hành động</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($locations as $value)
                                    <tr>
                                        <td>
                                            <div class="form-check style-1">
                                                <input class="form-check-input" type="checkbox" value="">
                                            </div>
                                        </td>
                                        <td class="job-desk3">
                                            <p class="mb-0">{{$value->name}}</p>
                                        </td>
                                        <td>
                                            <div>
                                                <span class="font-w600 text-nowrap">
                                                    <i class="flaticon-381-location me-2"></i>
                                                    {{$value->province->_name}}
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="font-w600 text-nowrap">{{timeAgo($value->created_at)}}</span>
                                        </td>
                                        <td>
                                            <button class="btn btn-sm btn-danger" data-id="{{ $value->id }}" data-action="{{ route('admin.location.delete',$value->id) }}" onclick="deleteConfirmation({{$value->id}})">Xoá</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-between">
                                    <div class="dataTables_info" id="guestTable-all_info" role="status" aria-live="polite"></div>
                                    <div class="dataTables_paginate paging_simple_numbers">
                                        <nav>
                                            @include('main.component.pagination',['datas' => $locations])
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('main')}}/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="{{asset('main')}}/js/plugins-init/sweetalert.init.js"></script>
    <script type="text/javascript">
        function deleteConfirmation(id) {
            swal({
                title: "Delete?",
                text: "Please ensure and then confirm!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: !0
            }).then(function (e) {
                if (e.value === true) {
                    $.ajax({
                        type: 'DELETE',
                        url: "{{url('/admin/locations')}}/" + id + "/delete",
                        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                        dataType: 'JSON',
                        success: function (results) {
                            location.reload();
                            if (results) {
                                swal("Done!", results.message, "success");
                            } else {
                                swal("Error!", results.message, "error");
                            }
                        }
                    });

                } else {
                    e.dismiss;
                }

            }, function (dismiss) {
                return false;
            })
        }
    </script>
@endsection

