@extends('layouts.main')
@section('title', $title)
@section('styles')
    <style>
        .input-custom-filter{
            padding: 0 10px !important;
            height: 2.5rem !important;
            border-radius: 5px !important;
        }
        .nice-select.line-height-custom {
            line-height: 34px !important;
        }
    </style>
@endsection
@section('contents')
    <div class="row mt-4">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="tab-content">
                        <div class="tab-pane active show" id="AllEmployee">
                            <div class="table-responsive">
                                <button class="btn btn-warning btn-sm m-3" id="headingOne" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-controls="collapseOne" aria-expanded="true"><i class="flaticon-034-filter"></i></button>
                                <div class="collapse row m-2 {{$oldStatus || $oldKeyword ? 'show' : ''}}" id="collapseOne" aria-labelledby="headingOne" data-bs-parent="#accordion-one">
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label class="form-label">Keyword search</label>
                                            <input type="text" class="form-control input-default input-custom-filter" id="keyword" placeholder="Enter your keyword search data" value="{{$oldKeyword ?? ''}}">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="mb-3">
                                            <label class="form-label">Status:</label>
                                            <select class="me-sm-2 default-select form-control wide input-custom-filter line-height-custom" id="status" style="display: none;">
                                                <option value="">Choose...</option>
                                                <option value="2" {{$oldStatus == ACTIVE ? 'selected' : ''}}>Active</option>
                                                <option value="1" {{$oldStatus == INACTIVE ? 'selected' : ''}}>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div style="margin-top: 1.7rem !important;">
                                            <button class="btn btn-sm btn-warning" id="filter-group-client"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <table class="table card-table display mb-4 shadow-hover default-table table-responsive-lg dataTable no-footer" id="guestTable-all">
                                    <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th class="bg-none">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $value)
                                    <tr>
                                        <td>
                                            <div class="concierge-list-bx d-flex align-items-center">
                                                <img class="me-3 rounded" src="{{$value->image}}" alt="">
                                                <div>
                                                    <h5 class="fs-16 mb-0 text-nowrap"><a class="text-black" href="javascript:void(0);">{{$value->user_name}}</a></h5>
                                                    <span class=" text-secondary fs-14 d-block">{{$value->name}}</span>
                                                    <span class=" fs-14  text-nowrap">Đăng ký {{timeAgo($value->created_at)}}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <span class="font-w600 text-nowrap">
                                                    <i class="fas fa-phone-alt me-2 "></i>
                                                    {{$value->email}}
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <span  class="font-w600 {{$value->status == ACTIVE ? "text-success" : "text-danger"}}">
                                                {{$value->status == ACTIVE ? "ACTIVE" : "INACTIVE"}}
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{route('admin.client.edit', $value->id)}}" class="btn btn-sm {{$value->status == ACTIVE ? "btn-danger" : "btn-success"}}">
                                                {{$value->status == ACTIVE ? "INACTIVE" : "ACTIVE"}}
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-between">
                                    <div class="dataTables_info" id="guestTable-all_info" role="status" aria-live="polite"></div>
                                    <div class="dataTables_paginate paging_simple_numbers">
                                        <nav>
                                            @include('main.component.pagination',['datas' => $users])
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#filter-group-client').on('click',function(e) {
            var keyword = $('#keyword').val();
            var status = $('#status').val();
            window.location.replace("?keyword=" + keyword + "&status=" + status)
        });
    </script>
@endsection

