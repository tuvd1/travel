@if (isset($datas) && $datas->lastPage() > 1)

        <ul class="pagination pagination-gutter pagination-primary no-bg">

        <?php
        $interval = isset($interval) ? abs(intval($interval)) : 3 ;
        $from = $datas->currentPage() - $interval;
        if($from < 1){
            $from = 1;
        }

        $to = $datas->currentPage() + $interval;
        if($to > $datas->lastPage()){
            $to = $datas->lastPage();
        }
        ?>

        <!-- first/previous -->
            @if($datas->currentPage() > 1)
                <li class="page-item page-indicator">
                    <a href="{{ $datas->url(1) }}" aria-label="First" class="page-link">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>

                <li class="page-item page-indicator">
                    <a href="{{ $datas->url($datas->currentPage() - 1) }}" class="page-link" aria-label="Previous">
                        <span aria-hidden="true">Prev</span>
                    </a>
                </li>
            @endif

        <!-- links -->
            @for($i = $from; $i <= $to; $i++)
                <?php
                $isCurrentPage = $datas->currentPage() == $i;
                ?>
                <li class="page-item {{ $isCurrentPage ? 'active' : '' }}">
                    <a href="{{ !$isCurrentPage ? $datas->url($i) : '#' }}" class="page-link">
                        {{ $i }}
                    </a>
                </li>
            @endfor

        <!-- next/last -->
            @if($datas->currentPage() < $datas->lastPage())
                <li class="page-item page-indicator">
                    <a href="{{ $datas->url($datas->currentPage() + 1) }}" class="page-link" aria-label="Next">
                        <span aria-hidden="true">Next</span>
                    </a>
                </li>

                <li class="page-item page-indicator">
                    <a href="{{ $datas->url($datas->lastpage()) }}" class="page-link" aria-label="Last">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            @endif

        </ul>

    @endif
