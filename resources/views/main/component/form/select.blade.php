<div class="mb-3">
    <label class="fs-16 ms-1" for="{{$id}}">
        {{$label}}
        <span class="text-danger">*</span>
    </label>
    <select class="default-select form-control wide mb-3" id="{{$id}}" name="{{$name}}">
        <option value="">Please choose an option!</option>
        @foreach($options as $item)
        <option value="{{$item->id}}" @if(isset($value)) {{$value == $item->id ? 'selected' : ''}} @endif>{{$item->name}}</option>
        @endforeach
    </select>
    @error($name)
    <p class="text-danger mt-1">{{ $message }}</p>
    @enderror
</div>
