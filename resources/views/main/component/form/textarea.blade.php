<div class="mb-3">
    <label class="fs-16 ms-1" for="{{$id}}">
        {{$label}}
        <span class="text-danger">*</span>
    </label>
    <textarea class="form-control" rows="4" name="{{$name}}" id="{{$id}}" placeholder="{{$placeholder}}">{{$value ?? ""}}</textarea>
    @error($name)
    <p class="text-danger mt-1">{{ $message }}</p>
    @enderror
</div>
