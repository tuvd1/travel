@extends('layouts.main')
@section('title', $title)
@section('styles')
    <link rel="stylesheet" href="{{asset('main')}}/vendor/select2/css/select2.min.css">
    <link href="{{asset('main')}}/vendor/jquery-nice-select/css/nice-select.css" rel="stylesheet">
@endsection
@section('contents')
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-body">
                    <div class="basic-form">
                        <form action="{{$action}}" method="post" enctype="multipart/form-data">
                            {{isset($method) ? method_field('PUT') : ""}}
                            @csrf
                            <div class="row mt-2">
                                <div class="col-xl-4 col-lg-4">
                                    @include('main.component.form.input',
                                        [
                                            'label' => 'Name',
                                            'id' => "name",
                                            'placeholder' => "Please enter name asset!",
                                            'type' => "text",
                                            'name' => "name",
                                            'value' => $asset->name ?? "",
                                        ]
                                    )
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <div class="mb-3">
                                        <label class="fs-16 ms-1" for="single-select2">
                                           Province
                                            <span class="text-danger">*</span>
                                        </label>
                                        <select class="default-select form-control wide mb-3" id="single-select2" name="province">
                                            <option value="">Please choose an option!</option>
                                            @foreach($province as $item)
                                                <option value="{{$item->name}}" @if(isset($asset)) {{$asset->province == $item->name ? 'selected' : ''}} @endif>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('province')
                                        <p class="text-danger mt-1">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <div class="mb-3">
                                        <label class="fs-16 ms-1" for="single-select">
                                            District
                                            <span class="text-danger">*</span>
                                        </label>
                                        <select class="default-select form-control wide mb-3" id="single-select" name="district">
                                            <option value="">Please choose an option!</option>
                                            @foreach($district as $item)
                                                <option value="{{$item->name}}" @if(isset($asset)) {{$asset->district == $item->name ? 'selected' : ''}} @endif>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('province')
                                        <p class="text-danger mt-1">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-xl-12 col-lg-12">
                                    @include('main.component.form.textarea',
                                        [
                                            'label' => 'Address',
                                            'id' => "address",
                                            'placeholder' => "Please enter address!",
                                            'name' => "address",
                                            'value' => $asset->address ?? "",
                                        ]
                                    )
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-xl-12 col-lg-12">
                                    <label for="id">
                                        Image
                                        <span class="text-danger">*</span>
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="form-file">
                                            <input type="file" id="image" name="image" class="form-file-input form-control">
                                        </div>
                                        <span class="input-group-text">Upload</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-xl-4 col-lg-4">
                                    <button class="btn btn-success bg-primary">Save</button>
                                    <a href="{{route('rooms.index')}}" class="btn btn-dark">Cancel</a>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('main')}}/vendor/jquery-nice-select/js/jquery.nice-select.min.js"></script>
    <script src="{{asset('main')}}/vendor/select2/js/select2.full.min.js"></script>
    <script src="{{asset('main')}}/js/plugins-init/select2-init.js"></script>

@endsection
