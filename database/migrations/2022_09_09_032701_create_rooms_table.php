<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->integer('bed')->nullable();
            $table->integer('television')->nullable();
            $table->integer('fridge')->nullable();
            $table->integer('bathroom')->nullable();
            $table->integer('amount_of_people')->nullable();
            $table->tinyInteger('breakfast')->nullable();
            $table->tinyInteger('breakfast_details')->nullable();
            $table->text('description');
            $table->integer('area');
            $table->tinyInteger('status')->default(1);
            $table->integer('price');
            $table->bigInteger('asset_id');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
};
