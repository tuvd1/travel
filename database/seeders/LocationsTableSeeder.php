<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('locations')->insert([
                'name' => $faker->name,
                'province_id' => $faker->numberBetween(1, 63)
            ]);
        }
    }
}
