<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $limit = 10;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('rooms')->insert([
                'bed' => $faker->numberBetween(1,3),
                'television' => 1,
                'fridge' => 1,
                'bathroom' => 1,
                'amount_of_people' => $faker->numberBetween(1,5),
                'description' => "no text",
                'status' => $faker->numberBetween(1,2),
                'price' => $faker->randomNumber(),
                'area' => $faker->numberBetween(25,70),
                'asset_id' => $faker->numberBetween(1,20)
            ]);
        }
    }
}
