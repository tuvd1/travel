<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([
                'name' => $faker->name(),
                'user_name' => $faker->userName(),
                'email' => $faker->email,
                'phone' => $faker->phoneNumber(),
                'password' => $faker->password(),
                'birth_day' => $faker->date(),
                'address' => $faker->address(),
                'role' => $faker->numberBetween(1, 2),
                'image' => $faker->imageUrl() ,
            ]);
        }
    }
}
