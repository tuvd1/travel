<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'bed' => 'required|numeric|min:1',
            'television' => 'required|numeric|min:0',
            'fridge' => 'required|numeric|min:1',
            'bathroom' => 'required|numeric|min:1',
            'amount_of_people' => 'required|numeric|min:1',
            'area' => 'required|numeric|min:1',
            'description' => 'required',
            'price' => 'required|numeric|min:1',
            'asset_id' => 'required',
            'breakfast' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'asset_id.required' => 'The property field is required.',
            'amount_of_people.required' => 'The mzaximum number of people field is required.',
        ];
    }
}
