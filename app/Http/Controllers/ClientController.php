<?php

namespace App\Http\Controllers;

use App\Models\AssetModel;
use App\Models\BookingModel;
use App\Models\ImageAssetModel;
use App\Models\RoomModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;



class ClientController extends Controller
{
    protected $user;
    protected $asset;
    protected $room;
    protected $image;
    protected $booking;
    public function __construct(User $user, AssetModel $asset, RoomModel $room, ImageAssetModel $image, BookingModel $booking)
    {
        $this->user = $user;
        $this->asset = $asset;
        $this->room = $room;
        $this->image = $image;
        $this->booking = $booking;
    }
    public function home()
    {
        $title = 'Home page';
        $rooms = $this->room
            ->with('asset', 'image')
            ->whereDoesntHave('bookings', function ($query){
                return $query->where('status' ,'=', 2);
            })
            ->get();
        return view('clients.homepage', compact('title', 'rooms'));
    }

    public function booking()
    {
        $oldProvince = request()->get('province');
        $oldPeople = request()->get('people');

        $assets = $this->asset->get();
        foreach ($assets as $value)
        {
            $newArray[$value['province']] = $value;
        }
        $assets = $newArray;
        $booking = $this->booking->select('room_id')->get();
        $title = 'Booking';
        $keyword = request()->get('province');
        $rooms = $this->room->with('asset', 'image')
            ->with('asset', function ($query)use ($keyword){
                if (request()->get('province')) {
                    $query->where('province', 'LIKE', '%'.$keyword.'%');
                }
            })
            ->whereHas('asset', function ($query){
                if (request()->get('people')) {
                    $query->where('amount_of_people', '<=', request()->get('people'));
                }
            })
            ->whereDoesntHave('bookings', function ($query){
                return $query->where('status' ,'=', 2);
            })
            ->orderBy('id')->paginate(10);
        return view('clients.booking', compact('title', 'rooms', 'assets', 'booking', 'oldPeople', 'oldProvince' ));
    }

    public function roomShow($id)
    {
        $room = $this->room->where('id',$id)->first()->load('asset', 'image');
        $title = 'Room detail';
        return view('clients.room_detail', compact('title', 'room'));
    }

    public function bookingStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date|after:today',
            'end_date' => 'required|date|after:start_date',
            'phone' => 'required|unique:users'
        ]);

        if ($validator->fails()) {
            $responseArr = $validator->errors();
            return response()->json(["message" => $responseArr, "status" => false]);
        } else {
            $fullname = $request->first_name. " " .$request->last_name;
            $this->user->find(Auth::id())->update([
                'name' => $fullname,
                'phone' => $request->phone,
            ]);
            $bookings = $this->booking->create([
                'code' => mt_rand(10000000,99999999),
                'total_money' => $request->total_price,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'user_id' => Auth::id(),
                'room_id' => $request->room_id,
            ]);
            return response()->json(["message" => "Booking success", 'data_booking' => $bookings, "status" => true]);
        }

    }
}
