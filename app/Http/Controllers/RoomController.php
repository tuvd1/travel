<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoomRequest;
use App\Models\AssetModel;
use App\Models\ImageAssetModel;
use App\Models\RoomModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoomController extends Controller
{
    protected $user;
    protected $asset;
    protected $room;
    protected $image;
    public function __construct(User $user, AssetModel $asset, RoomModel $room, ImageAssetModel $image)
    {
        $this->user = $user;
        $this->asset = $asset;
        $this->room = $room;
        $this->image = $image;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Room list";
        $rooms = $this->room->with('asset', 'image')->orderBy('id')->paginate(1);
        return view('main.partner.room.list', compact('rooms', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = route('rooms.store');
        $title = "Room create";
        $assets = $this->asset->select('id', 'name')->get();
        $optionBreakfast = json_decode(json_encode(config('common.breakfast'), true));
        return view('main.partner.room.create_or_update', compact('assets', 'optionBreakfast', 'title', 'action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoomRequest $request)
    {
        try {
            $roomStore = $this->room->updateOrCreate(
                [
                    'bed' => $request->bed,
                    'television' => $request->television,
                    'fridge' => $request->fridge,
                    'bathroom' => $request->bathroom,
                    'amount_of_people' => $request->amount_of_people,
                    'area' => $request->area,
                    'description' => $request->description,
                    'price' => $request->price,
                    'asset_id' => $request->asset_id,
                    'breakfast' => $request->breakfast
                ]
            );
            foreach (json_decode($request->image) as $image){
                $this->image->find($image)->update([
                    'asset_id' => $roomStore->id
                ]);
            }
        } catch (\Throwable $e) {
            return back()->with('message', $e)->with('status', false);
        }

        return redirect()->route('rooms.index')->with('message', 'create room success')->with('status', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action = route('rooms.update', $id);
        $method = "put";
        $room = $this->room->find($id);
        $title = "Room update";
        $assets = $this->asset->select('id', 'name')->get();
        $optionBreakfast = json_decode(json_encode(config('common.breakfast'), true));
        return view('main.partner.room.create_or_update', compact('title', 'assets', 'optionBreakfast', 'room', 'action', 'method'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoomRequest $request, $id)
    {
        try {
            $roomStore = $this->room->find($id)->updateOrCreate(
                [
                    'id' => $id
                ],
                [
                    'bed' => $request->bed,
                    'television' => $request->television,
                    'fridge' => $request->fridge,
                    'bathroom' => $request->bathroom,
                    'amount_of_people' => $request->amount_of_people,
                    'area' => $request->area,
                    'description' => $request->description,
                    'price' => $request->price,
                    'asset_id' => $request->asset_id,
                    'breakfast' => $request->breakfast
                ]
            );
            foreach (json_decode($request->image) as $image) {
                $this->image->find($image)->update([
                    'asset_id' => $roomStore->id
                ]);
            }
        } catch (\Throwable $e) {
            return back()->with('message', $e)->with('status', false);
        }

        return redirect()->route('rooms.index')->with('message', 'update room success')->with('status', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->room->find($id)->delete();
        } catch (\Throwable $e) {
            return back()->with('message', $e)->with('status', false);
        }

        return redirect()->route('rooms.index')->with('message', 'remove room success')->with('status', true);
    }
}
