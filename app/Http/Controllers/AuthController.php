<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function login(Request $request)
    {
        $login = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if (Auth::attempt($login)) {
            if (Auth::user()->status == ACTIVE) {
                return response()->json(['status' => true]);
            }else{
                Auth::logout();
                return response()->json(['status' => false, 'message' => 'Your account has been locked!']);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'Incorrect account or password']);
        }
    }

    public function register(Request $request)
    {
        $user = $this->user->create([
            'name' => '',
            'user_name' => $request->name,
            'email' => $request->email,
            'phone' => '',
            'image' => "https://www.pinclipart.com/picdir/middle/221-2217551_computer-user-clip-art.png",
            'password' => Hash::make($request->password),
        ]);
        Auth::login($user);
        return response()->json($user);
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('client.homepage'));
    }

}
