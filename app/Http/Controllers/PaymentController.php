<?php

namespace App\Http\Controllers;

use App\Models\BookingModel;
use App\Models\PaymentModel;
use App\Models\RoomModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    protected $booking;
    protected $user;
    protected $room;
    protected $payment;
    public function __construct(User $user, RoomModel $room, BookingModel $booking, PaymentModel $payment)
    {
        $this->user = $user;
        $this->room = $room;
        $this->booking = $booking;
        $this->payment = $payment;
    }
    public function viewPaymentBooking($code)
    {
        $title = 'Payment';
        $booking = $this->booking->where('code', $code)->with('user')->first();
        return view('clients.payment', compact('title', 'booking'));
    }

    public function checkout(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $vnp_TmnCode = "QMFDJ97M"; //Website ID in VNPAY System
        $vnp_HashSecret = "PNKXSYMUJHDSIIEXASCZQNHHLGYMZPHC"; //Secret key
        $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
        $vnp_Returnurl = route('client.booking.payment.return');

        $vnp_TxnRef = mt_rand(10000000,99999999); //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = $request->infor;
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = $request->amount*100;
        $vnp_Locale = 'vn';
        $vnp_BankCode = "NCB";
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

        $inputData = array(
            "vnp_Version" => "2.1.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        if (isset($vnp_Bill_State) && $vnp_Bill_State != "") {
            $inputData['vnp_Bill_State'] = $vnp_Bill_State;
        }

        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
            $vnpSecureHash =   hash_hmac('sha512', $hashdata, $vnp_HashSecret);//
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }
        $returnData = array('code' => '00'
        , 'message' => 'success'
        , 'data' => $vnp_Url);
        if (isset($_POST['redirect'])) {
            header('Location: ' . $vnp_Url);
            die();
        } else {
            echo json_encode($returnData);
        }
    }

    public function paymentRedirect()
    {
        $responseCode = request()->get('vnp_ResponseCode');
        if ($responseCode){
            $this->payment->create([
                'code' => request()->get('vnp_TxnRef'),
                'amount' => request()->get('vnp_Amount')/100,
                'type' => 'VN PAY',
                'status' => request()->get('vnp_ResponseCode'),
                'user_id' => Auth::id(),
                'booking_code' => preg_replace("/[^0-9]/","",request()->get('vnp_OrderInfo')),
                'description' => request()->get('vnp_OrderInfo'),
            ]);
            if (request()->get('vnp_ResponseCode') == VNP_SUCCESS) {
                $this->booking->where('code', preg_replace("/[^0-9]/","",request()->get('vnp_OrderInfo')))->update([
                    'status' => PAID
                ]);
            }
            $title = 'Payment status';
            return view('clients.payment_redirect', compact('title'));
        }
        return redirect(back());
    }
}
