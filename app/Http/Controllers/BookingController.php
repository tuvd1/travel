<?php

namespace App\Http\Controllers;

use App\Models\BookingModel;
use App\Models\ImageAssetModel;
use App\Models\RoomModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    protected $user;
    protected $room;
    protected $booking;
    protected $image;
    public function __construct(User $user, RoomModel $room, BookingModel $booking, ImageAssetModel $image)
    {
        $this->user = $user;
        $this->room = $room;
        $this->booking = $booking;
        $this->image = $image;
    }
    public function index()
    {
        $title = "List booking";
        $bookings = $this->booking->with('room', 'user', 'image')->whereHas('room', function ($query){
            $query->where('user_id', Auth::id());
        })->paginate(5);
        return view('main.partner.bookings.index', compact('bookings', 'title'));
    }

    public function bookings()
    {
        $keyword = request()->get('keyword');
        $title = "List booking";
        $bookings = $this->booking->where('user_id', Auth::id())
            ->where('code', 'like', '%'.$keyword.'%')
            ->with('image', 'room')
            ->paginate(10);
        return view('clients.list_booking', compact('title', 'bookings'));
    }
}
