<?php

namespace App\Http\Controllers;

use App\Models\AssetModel;
use App\Models\DistrictModel;
use App\Models\ImageAssetModel;
use App\Models\ProvinceModel;
use App\Models\RoomModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AssetController extends Controller
{
    protected $user;
    protected $asset;
    protected $room;
    protected $province;
    protected $district;
    protected $image;
    public function __construct(User $user, AssetModel $asset, RoomModel $room, ProvinceModel $province, DistrictModel $district, ImageAssetModel $image)
    {
        $this->user = $user;
        $this->asset = $asset;
        $this->room = $room;
        $this->province = $province;
        $this->district = $district;
        $this->image = $image;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'List asset';
        $assets = $this->asset->where('user_id', Auth::id())->with('image')->orderBy('id')->paginate(1);
        return view('main.partner.assets.list', compact('assets','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = route('assets.store');
        $province = $this->province->get();
        $district = $this->district->get();
        $title = "Create asset";
        return view('main.partner.assets.create_or_update', compact('title','action', 'province', 'district'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $assetStore = $this->asset->create([
                'name' => $request->name,
                'province' => $request->province,
                'district' => $request->district,
                'address' => $request->address,
                'user_id' => Auth::id(),
            ]);
            $this->image->create([
                "image_path" => uploadFile($request->image),
                "asset_id" => $assetStore->id
            ]);
            return redirect()->route('assets.index')->with('message', 'create asset success')->with('status', true);
        } catch (\Throwable $e) {
            return back()->with('message', $e)->with('status', false);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Update asset";
        $action = route('assets.update', $id);
        $method = 'put';
        $province = $this->province->get();
        $district = $this->district->get();
        $asset = $this->asset->find($id);
        return view('main.partner.assets.create_or_update', compact('title', 'action', 'province', 'district', 'asset', 'method'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->asset->find($id)->update([
                'name' => $request->name,
                'province' => $request->province,
                'district' => $request->district,
                'address' => $request->address,
            ]);
            if ($request->image) {
                $this->image->where('asset_id', $id)->update([
                    "image_path" => uploadFile($request->image),
                ]);
            }
            return redirect()->route('assets.index')->with('message', 'update asset success')->with('status', true);
        } catch (\Throwable $e) {
            return back()->with('message', $e)->with('status', false);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->asset->find($id)->delete();
        } catch (\Throwable $e) {
            return back()->with('message', $e)->with('status', false);
        }

        return redirect()->route('assets.index')->with('message', 'remove asset success')->with('status', true);
    }
}
