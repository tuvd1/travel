<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'bookings';
    protected $fillable = [
        'code',
        'total_money',
        'start_date',
        'end_date',
        'user_id',
        'room_id'
    ];

    public function room()
    {
        return $this->belongsTo(RoomModel::class, 'room_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function image()
    {
        return $this->belongsTo(ImageAssetModel::class, 'room_id', 'id');
    }
}
