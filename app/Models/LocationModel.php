<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocationModel extends Model
{
    use HasFactory;
    protected $table = "locations";
    protected $fillable = ['name', 'province_id'];

    public function province()
    {
        return $this->belongsTo(ProvinceModel::class, 'province_id', 'id');
    }
}
